# SecondImage Architecture

## Introduction

This document is intended to describe some aspects of the architecture of the
SecondImage code bases.

## General

The following table gives a brief overview of each of the Haskell projects.

|Project Name|Description|
|------------|-----------|
|[bpdf-server](https://bitbucket.org/keaisdev/bpdf-server)|A service that aggregates individual PDF pages that have already been charted into a single PDF document.|
|[burn-cd-server](https://bitbucket.org/keaisdev/burn-cd-server)|Creates CD images with record files for burn cd equipment.|
|[cust-chart-server](https://bitbucket.org/keaisdev/cust-chart-server)|Creates records that were custom charted by customers.|
|[db-service](https://bitbucket.org/keaisdev/db-service)|Daily update of various tables in sql server, examples are deleting old records or computing some values, etc.|
|[dicom-haskell-library](https://bitbucket.org/keaisdev/dicom-haskell-library)|A library for reading DICOM image files. This is a fork of [a library on Hackage](https://hackage.haskell.org/package/dicom).|
|[edt-server](https://bitbucket.org/keaisdev/edt-server)|Electronic transfer of records to some of our customers.|
|[email-gateway](https://bitbucket.org/keaisdev/email-gateway)|A gateway for all our services to send email via http API in order to avoid setting every vm with postfix relay.|
|[email-notif](https://bitbucket.org/keaisdev/email-notif)|Shipping email notifications.|
|[ephesoft](https://bitbucket.org/keaisdev/ephesoft)|Parsing outout of ephesoft software.|
|[hdbc-odbc](https://bitbucket.org/keaisdev/hdbc-odbc)|A library for interfacing with databases in Haskell. This is an old fork of [a library on Hackage](https://hackage.haskell.org/package/HDBC-odbc).|
|[hs-util](https://bitbucket.org/keaisdev/hs-util)|Library that contains generally useful funtionality used by some of the other projects.|
|[hxray](https://bitbucket.org/keaisdev/hxray)|Parsing dicom files and extracting jepgs out of them for online view.|
|[keais-import](https://bitbucket.org/keaisdev/keais-import)|Imports workorders from Keais system.|
|[ocr-server](https://bitbucket.org/keaisdev/ocr-server)|A service that performs optical character recoginition (OCR) on individual PDF pages.|
|[pdf-server](https://bitbucket.org/keaisdev/pdf-server)|Service that provides various pdf artifacts like cover page or Table of Contents to Sirus.|
|[record-toc](https://bitbucket.org/keaisdev/record-toc)|Creates a table of contents (TOC) from the "records" PDF.|
|[si-pdflib](https://bitbucket.org/keaisdev/si-pdflib)|Library to work with pdf via FFI to pdflib commercial library.|
|[si-pdflib-templates](https://bitbucket.org/keaisdev/si-pdf-templates)|Common PDF building code like coverpages etc, used by several other applications.|
|[sirus2](https://bitbucket.org/keaisdev/sirus2)|Web UI for Sirus, for example various task queues, OPAQ, charting, etc.|
|[sirus-server](https://bitbucket.org/keaisdev/sirus-server)|JSON API server for Sirus.|
|[si](https://bitbucket.org/keaisdev/si/)|SecondImage Portal website.|
|[si-intranet](https://bitbucket.org/keaisdev/si-intranet/)|Performs some internal tasks that are needed. Many of these tasks have been migrated to haskell services, but a few remain - specifically daily notification emails. One thing this app does is facilitate batch printing from Sirus app. See /packet-task endpoint.|
|[si-sso](https://bitbucket.org/keaisdev/si-sso)|Single Sign On for SecondImage web site.|
|[si-web](https://bitbucket.org/keaisdev/si-web)|Customizer and some other web modules for SecondImage web site.|
|[smartchron-server](https://bitbucket.org/keaisdev/smartchron-server)|Creates smartchron records for keais.|
|[split-pdf-server](https://bitbucket.org/keaisdev/split-pdf-server)|Splits large pdfs based on customer properties into smaller volumes.|
|[tp-chart](https://bitbucket.org/keaisdev/tp-chart)|Third Party Charting.|

## Dependencies

The following diagram shows dependencies between the Haskell applications (shown
in rectangles) and Haskell libraries (shown as ellipses).  Note that third-party
dependencies are not shown - just internal ones.

![Application Dependencies](img/dependencies.png)

## Docker

Some, but not all, of the projects are built using Docker.  The following
diagram shows the relationship between each project's docker image with the
images on which they are based.

![Docker Images Dependencies](img/docker-builds.png)
