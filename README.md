# SecondImage Architecture

This repo is intended to be a source of information about the architecture of
the various applications from SecondImage.

## Building

This project uses gnu make and graphviz for building.  Please ensure that they
are installed before building.

To build just run `make` in the project directory.

## Contents

* [Server Architecture](SERVER_ARCHITECTURE.md)
* [Applications Architecture](ARCHITECTURE.md)
* [Database](DATABASE.md)
