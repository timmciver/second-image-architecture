# Database

This document gives information about the database used by the Haskell
applications.  This database is often referred to a the "SIRUS" database.

Note that this document is far from exhaustive; it only documents what has been
gleaned by the maintainers of the Haskell applications.

## Tables

The following diagram shows some of the tables used by the Haskell applications
and some of the relationships between them.

![SIRUS Database](img/database.png)
